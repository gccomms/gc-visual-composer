<?php
/**
 * Plugin Name: GC Visual Composer Elements
 * Description: Define new custom elements for Visual Composer, used on the Graduate Center web properties.
 * Version: 1.1.2
 * Author: The Graduate Center, Office Of Communications and Marketing
 * License: GPL2
 */

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class gc_visual_composer{
	public static function init(){

		// Display a notice if Page Builder is not installed or activated.
		if ( !defined( 'WPB_VC_VERSION' ) ) { 
			add_action( 'admin_notices', array( __CLASS__, 'vc_required_notice' ) );
			return;
		}

		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'admin_enqueue_scripts_and_styles' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'move_visual_composer_styles_to_head' ) );

		// VC Elements, in alphabetical order
		add_shortcode( 'gc_page_include', array( __CLASS__, 'page_include_shortcode' ) );
		add_shortcode( 'gc_post_list', array( __CLASS__, 'post_list_shortcode' ) );

		// Map shortcodes in Visual Composer
		add_action( 'vc_after_mapping', array( __CLASS__, 'shortcode_map' ) );

		// Register widgets
		add_action( 'widgets_init', array( __CLASS__, 'register_widgets' ) );

		// Remove 'Private' and 'Protected' for post titles
		add_filter( 'private_title_format', array( __CLASS__, 'remove_prefix_from_title' ) );
		add_filter( 'protected_title_format', array( __CLASS__, 'remove_prefix_from_title' ) );

		// Add the current user's role to the body classes
		if ( is_user_logged_in() ) {
			add_filter( 'body_class', array( __CLASS__, 'admin_body_class' ) );	
		}

		add_filter( 'admin_body_class', array( __CLASS__, 'admin_body_class' ) );
	}

	public static function vc_required_notice() {
		$plugin_data = get_plugin_data( __FILE__ );
		$output = sprintf( '<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">WP Bakery Page Builder v5.x or newer</a></strong>.', $plugin_data[ 'Name' ] );
		echo "<div class='updated'><p>$output</p></div>";
	}

	public static function remove_autop() {
		if ( !is_singular() ) {
			return;
		}

		// Remove WordPress' autop function for CPTs that use Visual Composer
		$vc_post_types = vc_manager()->editorPostTypes();
		if ( in_array( $GLOBALS['post']->post_type, $vc_post_types ) ) {
			remove_filter( 'the_content', 'wpautop' );
		}
	}

	public static function admin_enqueue_scripts_and_styles( $hook ) {
		wp_register_style( 'gc_vc_style', plugins_url( 'css/admin-style.css', __FILE__ ), array( 'js_composer' ), null );
		wp_enqueue_style( 'gc_vc_style' );

		if ( $hook == 'post.php' ) {
			wp_register_script( 'gc_vc_script', plugins_url( '/js/admin-functions.js', __FILE__ ), array( 'js_composer' ), null, true );
			$cuny_vc_params = array(
				'disable_sortable' => var_export( !current_user_can( 'delete_pages' ) && !current_user_can( 'cuny_editor_vc' ), true )
			);
			wp_localize_script( 'gc_vc_script', 'gc_vc_params', $cuny_vc_params );
			wp_enqueue_script( 'gc_vc_script' );
		}
	}

	// Enqueued styles in the HEAD section of the page
	public static function move_visual_composer_styles_to_head() {
		wp_enqueue_style( 'js_composer_front' );
		wp_enqueue_style( 'js_composer_custom_css' );
	}
	
	//
	// --- Shortcodes ------------------------------------------------
	//

	// [gc_page_include post_id="foo-value" el_class="class_name"]
	public static function page_include_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'post_id' => '0',
			'el_class' => ''
		), $atts ) );

		if ( empty( $post_id ) && is_numeric( $post_id ) ) {
			return '';
		}

		$page_include_obj = get_post( $post_id );

		if ( !is_object( $page_include_obj ) ) {
			return '';
		}

		$page_include_obj->post_content = str_replace( ']]>', ']]&gt;', $page_include_obj->post_content );
		$page_include_obj->post_content = do_shortcode( $page_include_obj->post_content );

		if ( !empty( $el_class ) ) {
			return "<div class='$el_class'>{$page_include_obj->post_content}</div>";
		}

		$shortcodes_custom_css = get_post_meta( $post_id, '_wpb_shortcodes_custom_css', true );
		if ( ! empty( $shortcodes_custom_css ) ) {
			$shortcodes_custom_css = strip_tags( $shortcodes_custom_css );
			$page_include_obj->post_content = '<style type="text/css" data-type="vc_shortcodes-custom-css">' . $shortcodes_custom_css . '</style>' . $page_include_obj->post_content;
		}

		return $page_include_obj->post_content;
	}

	public static function post_list_shortcode( $atts, $shortcode_content = null ) {
		extract( shortcode_atts( array(
			'loop' => 'size:10|order_by:date|order:DESC|post_type:post',

			'date_query_before' => '',
			'date_query_after' => '',
			'date_query_column' => '',

			'meta_key' => '',
			'meta_value' => '',
			'meta_compare' => '',
			'sort_by_meta_key' => "false",
			'offset' => 0,
			'exclude_post_ids' => '',

			'blog_id' => 0,
			'multisite_post_type' => '',
			'multisite_categories' => 0,
			'multisite_tags' => 0,
			'multisite_tax_query' => 0,

			'excerpt_length' => 0,
			'excerpt_more' => 'Read More',

			'wrapper_tag' => 'ul',
			'wrapper_id' => '',
			'el_class' => 'list-unstyled',

			'template' => base64_encode( '<li><a href="{{link}}">{{title}}</a></li>' )
		), $atts ) );

		// Is the multisite post_type defined?
		if ( !empty( $multisite_post_type ) ) {
			$multisite_post_type = str_replace( ' ', '', $multisite_post_type );
			if ( strpos( $loop, 'post_type' ) !== false ) {
				$loop = preg_replace( '/\|post_type:[a-zA-Z0-9_\-,]+/i',  '|post_type:' . $multisite_post_type, $loop );
			}
			else {
				$loop .= '|post_type:' . $multisite_post_type;
			}
		}

		// Is the multisite taxonomy query defined?
		$multisite_unregister_cpt = false;
		if ( !empty( $multisite_tax_query ) ) {
			$multisite_tax_query = str_replace( ' ', '', $multisite_tax_query );

			if ( strpos( $multisite_tax_query, ':' ) !== false ) {	
				list( $multisite_taxonomy_slug, $multisite_terms ) = explode( ':', $multisite_tax_query );

				// switch_to_blog doesn't call the plugins active on the other site, so the custom post type and taxonomy need to be temporarily defined
				if ( !post_type_exists( $multisite_post_type ) ) {
					$multisite_unregister_cpt = true;
					register_post_type( $multisite_post_type, array( 'public' => true ) );
					register_taxonomy( $multisite_taxonomy_slug, $multisite_post_type );
				}

				if ( strpos( $loop, 'tax_query' ) !== false ) {
					$loop = preg_replace( '/\|tax_query:[0-9,\-\s]+/i',  '|tax_query:' . $multisite_terms, $loop );
				}
				else {
					$loop .= '|tax_query:' . $multisite_terms;
				}
			}
			else {
				// This might be just a list of terms, which defaults to the 'category' taxonomy
				$loop .= '|categories:' . $multisite_tax_query;
			}
		}

		// Retrieve the query parameters
		if ( !empty( $blog_id ) ) {
			switch_to_blog( $blog_id );
		}

		list( $loop_atts, $wp_query ) = vc_build_loop_query( $loop );

		// Set up initial query attributes
		$args = array(
			'order' => !empty( $loop_atts[ 'order' ] ) ? sanitize_text_field( $loop_atts[ 'order' ] ) : 'ASC',
			'orderby' => !empty( $loop_atts[ 'orderby' ] ) ? sanitize_text_field( $loop_atts[ 'orderby' ] ) : 'date',
			'perm' => 'readable',
			'post_type' => $loop_atts[ 'post_type' ],
			'post_status' => 'publish'
		);

		// Date query
		if ( !empty( $date_query_before ) || !empty( $date_query_after ) ) {
			$initial_date_query = $date_query_top_lvl = array();

			$valid_date_columns = array( 'post_date', 'post_date_gmt', 'post_modified', 'post_modified_gmt' );

			// Date query 'before' argument
			$before = self::sanitize_date_time( $date_query_before );
			if ( !empty( $before ) ) {
				$initial_date_query[ 'before' ] = $before;
			}

			// Date query 'after' argument
			$after = self::sanitize_date_time( $date_query_after );
			if ( !empty( $after ) ) {
				$initial_date_query[ 'after' ] = $after;
			}

			// Date query 'column' argument
			if ( !empty( $date_query_column ) && in_array( $date_query_column, $valid_date_columns ) ) {
				$initial_date_query[ 'column' ] = $date_query_column;
			}

			// Bring in the initial date query
			if ( !empty( $initial_date_query ) ) {
				$date_query_top_lvl[] = $initial_date_query;
			}

			// Date queries
			$args[ 'date_query' ] = $date_query_top_lvl;
		}

		// Post Author
		if( !empty( $loop_atts[ 'author' ] ) ) {
			$args[ 'author' ] = sanitize_text_field( $loop_atts[ 'author' ] );
		}

		// Meta key (for ordering and filtering)
		if ( !empty( $meta_key ) ) {
			$args[ 'meta_key' ] = sanitize_text_field( $meta_key );

			// Meta value (for filtering)
			if ( !empty( $meta_value ) ) {
				// Support for dynamic values
				if ( strpos( $meta_value, 'date:' ) !== false ) {
					$meta_value = date( 'Y-m-d', strtotime( substr( $meta_value, 5 ) ) );
				}

				$args[ 'meta_query' ] = array(
					'key' => sanitize_text_field( $meta_key ),
					'value' => sanitize_text_field( $meta_value ),
					'compare' => !empty( $meta_compare ) ? html_entity_decode( sanitize_text_field( $meta_compare ) ) : '=',
					'type' => 'CHAR'
				);
			}
		}

		if ( filter_var( $sort_by_meta_key, FILTER_VALIDATE_BOOLEAN ) == true ) {
			$args[ 'orderby' ] = 'meta_value';
		}

		// Exclude Post IDs: unfortunate the built-in VC param doesn't seem to work with custom post types
		if ( !empty( $exclude_post_ids ) ) {
			$args[ 'post__not_in' ] = array_map( 'intval', explode( ',', $exclude_post_ids ) );
		}

		// Offset
		if( !empty( $offset ) ) {
			$args[ 'offset' ] = intval( $offset );
		}

		// Posts per page
		if ( !empty( $loop_atts[ 'posts_per_page' ] ) ) {
			$args[ 'posts_per_page' ] = intval( $loop_atts[ 'posts_per_page' ] );
		}

		// Categories and Tags
		if ( !empty( $multisite_categories ) ) {
			$args[ 'cat' ] = sanitize_text_field( $multisite_categories );
		}
		else if ( !empty( $loop_atts[ 'cat' ] ) ) {
			$args[ 'cat' ] = sanitize_text_field( $loop_atts[ 'cat' ] );
		}

		if ( !empty( $multisite_tags ) ) {
			$args[ 'tag__in' ] = array_map( 'intval', explode( ',', $multisite_tags ) );
		}
		else if ( !empty( $loop_atts[ 'tag__in' ] ) ) {
			$args[ 'tag__in' ] = array_map( 'intval', $loop_atts[ 'tag__in' ] );
		}

		// If taxonomy attributes, create a taxonomy query
		if ( !empty( $loop_atts[ 'tax_query' ] ) ) {
			$args[ 'tax_query' ] = $loop_atts[ 'tax_query' ];
		}

		// Build the new WP_Query
		$list = new WP_Query( $args );
		if ( !$list->have_posts() ) {
			return 'Sorry, no posts to display.';
		}

		// Decode the template
		$allowed_tags = '<li><br><p><a><div><span>';
		$template = rawurldecode( strip_tags( base64_decode( $template ), $allowed_tags ) );

		if ( empty( $template ) ) {
			$template = '<li><a href="{{link}}">{{title}}</a></li>';
		}

		// Find all the placeholders in the template, and calculate the values, including acf_fields and meta_fields
		// Placeholders can specify attributes, separated by colons: {{image:size:classes}}. Position determines which attribute each value is associated to
		preg_match_all( '~\{\{([a-z]+):?([a-z0-9\s\-\_]*)?:?([a-z0-9\s\-\_]*)?\}\}~i', $template, $matches );

		if ( empty( $matches[ 1 ] ) ) {
			return 'Invalid post template.';
		}

		$inner = '';
		while ( $list->have_posts() ) {
			$list->the_post();
			$placeholder_values = array();

			foreach ( $matches[ 1 ] as $idx => $a_key ) {
				switch ( $a_key ) {
					case 'acf':
						if ( empty( $matches[ 2 ][ $idx ] ) ) {
							$placeholder_values[] = '';
							break;
						}

						$field_object = get_field_object( $matches[ 2 ][ $idx ] );

						if ( empty( $field_object ) ) {
							$placeholder_values[] = '';
							break;
						}

						$placeholder_values[] = self::_render_acf_field( $field_object );
						break;

					case 'event':
						switch ( $matches[ 2 ][ $idx ] ) {
							case 'date':
								$start_date_time = get_post_meta( $list->post->ID, '_EventStartDate', true );
								$placeholder_values[] = date_i18n( 'F j, Y', strtotime( $start_date_time ) );
								break;

							case 'date_time':
								$start_date_time = get_post_meta( $list->post->ID, '_EventStartDate', true );
								$placeholder_values[] = date_i18n( 'F j, Y @ H:i', strtotime( $start_date_time ) );
								break;

							case 'venue':
								$venue_id = get_post_meta( $list->post->ID, '_EventVenueID', true );
								$placeholder_values[] = esc_html( get_the_title( $venue_id ) );
								break;

							default:
								$placeholder_values[] = '';
								break;

						}
						break;

					case 'author':
						$placeholder_values[] = get_the_author();
						break;

					case 'content':
						$placeholder_values[] = apply_filters( 'the_content', $GLOBALS[ 'post' ]->post_content );
						$placeholder_values[] = str_replace( ']]>', ']]&gt;', $placeholder_values[ $a_key ] );
						break;

					case 'date':
						$placeholder_values[] = get_the_date();
						break;

					case 'excerpt':
						// Custom build excerpt based on shortcode parameters
						if ( !empty( $excerpt_length ) || !empty( $excerpt_more ) || !empty( $excerpt_more_link ) ) {
							$length = !empty( $excerpt_length ) ? intval( $excerpt_length ) : apply_filters( 'excerpt_length', 55 );
							$more   = !empty( $excerpt_more ) ? $excerpt_more : apply_filters( 'excerpt_more', '' );
							$more   = '<a href="' . get_permalink() . '">' . $more . '</a>';

							if ( has_excerpt() ) {
								$placeholder_values[] = wp_trim_words( strip_shortcodes( $GLOBALS[ 'post' ]->post_excerpt ), $length, $more );
							}
							else {
								$placeholder_values[] = wp_trim_words( strip_shortcodes( $GLOBALS[ 'post' ]->post_content ), $length, $more );
							}
						}
						else {
							// Use default, can customize with WP filters
							$placeholder_values[] = get_the_excerpt();
						}
						break;

					case 'image':
						$image_size = !empty( $matches[ 2 ][ $idx ] ) ? $matches[ 2 ][ $idx ] : 'full';

						if ( has_post_thumbnail() ) {
							$attr = !empty( $matches[ 3 ][ $idx ] ) ? array( 'class' => $matches[ 3 ][ $idx ] ): array();
							$placeholder_values[] = get_the_post_thumbnail( get_the_ID(), $image_size, $attr );
						}
						else {
							$placeholder_values[] = '';
						}
						break;

					case 'link':
						$placeholder_values[] = get_permalink();
						break;

					case 'title':
						$placeholder_values[] = get_the_title();
						break;

					/* TO DO: add support for more placeholders: list of categories, list of tags, etc */

					default:
						/* TO DO: add support for regular custom fields */

						// unknown placeholder
						break;
				}
			}

			$inner .= str_replace( $matches[ 0 ], $placeholder_values, $template );
		}

		// Unregister 'temporary' custom post type and taxonomy
		if ( $multisite_unregister_cpt ) {
			unregister_taxonomy( $multisite_taxonomy_slug );
			unregister_post_type( $multisite_post_type );
		}

		// Restore the default WP Query object
		wp_reset_postdata();

		// Restore the current blog ID, if we fetched data from another blog in the network
		if ( !empty( $blog_id ) ) {
			restore_current_blog();
		}

		if( !empty( $wrapper_id ) ) {
			$wrapper_id = ' id="' . $wrapper_id . '"';
		}

		if( !empty( $el_class ) ){
			$el_class = ' class="' . $el_class . '"';
		}

		$open = '<div class="understrap-post-list wpb_content_element"><div class="wpb_wrapper"><' . $wrapper_tag . $wrapper_id . $el_class . '>';

		$close = '</' . $wrapper_tag . '></div></div>';

		return $open . $inner . $close;
	}

	private static function sanitize_date_time( $_date ) {
		if ( empty( $_date ) ) {
			return array();
		}

		$segments = array();

		/*
		* If $_date is not a strictly-formatted date or time, attempt to salvage it with
		* as strototime()-ready string. This is supported by the 'date', 'date_query_before',
		* and 'date_query_after' attributes.
		*/
		if ( false !== strpos( $_date, ' ' ) || false === strpos( $_date, '-' ) ) {
			if ( false !== $timestamp = strtotime( $_date ) ) {
				return $_date;
			}
		}

		$parts = array_map( 'absint', explode( '-', $_date ) );
		
		// Defaults to 2001 for years, January for months, and 1 for days.
		$year = $month = $day = 1;

		if ( count( $parts ) >= 3 ) {
			list( $year, $month, $day ) = $parts;

			$year  = ( $year  >= 1 && $year  <= 9999 ) ? $year  : 1;
			$month = ( $month >= 1 && $month <= 12   ) ? $month : 1;
			$day   = ( $day   >= 1 && $day   <= 31   ) ? $day   : 1;
		}

		return array(
			'year'  => $year,
			'month' => $month,
			'day'   => $day
		);
	}

	protected static function _render_acf_field( $_field ) {
		$output = '';

		if ( !is_array( $_field ) || empty( $_field[ 'type' ] ) ) {
			return $output;
		}
		switch ( $_field[ 'type' ] ) {
			case 'email':
				$output = '<a href="mailto:' . $_field[ 'value' ] . '">' . $_field[ 'value' ] . '</a>';
				break;

			case 'repeater':
				if ( empty( $_field[ 'value' ] ) ) {
					break;
				}

				$output = '<table class="table table-hover"><thead class="thead-light">';

				// Table headers
				foreach ( $_field[ 'value' ][ 0 ] as $a_column_key => $a_column_val ) {
					$output .= '<th scope="col">' . self::_get_field_label( $_field, $a_column_key ) . '</th>';
				}				
				$output .= '</thead>';

				// Table content
				while ( have_rows( $_field[ 'name' ] ) ) {
					$output .= '<tr>';
					$row = the_row();

					foreach ( $row as $a_column_slug => $a_column_val ) {
						$sub_field = get_sub_field_object( $a_column_slug );
						$output .= '<td>' . self::_render_acf_field( $sub_field ) . '</td>';
					}
					$output .= '</tr>';
				}
				reset_rows();

				$output .= '</table>';
				break;

			case 'select':
				$output = $_field[ 'choices' ][ $_field[ 'value' ] ];
				break;


			case 'url':
				$output = '<a href="' . $_field[ 'value' ] . '">' . $_field[ 'value' ] . '</a>';
				break;

			default:
				if ( stripos( $_field[ 'name' ], 'phone' ) !== false ) {
					// If this field has "phone" in its label, try to format it as a phone number
					$output = preg_replace( '~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '($1) $2-$3', $_field[ 'value' ] );
				}
				else {
					// Just use the value as is
					$output = $_field[ 'value' ];
				}
				break;
		}

		return $output;
	}

	protected static function _get_field_label( $_field, $_slug ) {
		if ( !is_array( $_field[ 'sub_fields' ] ) ) {
			return $_slug;
		}

		foreach ( $_field[ 'sub_fields' ] as $a_field ) {
			if ( $a_field[ 'name' ] == $_slug ) {
				return $a_field[ 'label' ];
			}
		}

		return $_slug;
	}

	//
	// --- Shortcode Mapping -----------------------------------------
	//

	public static function shortcode_map(){		
		vc_map( array(
			'name' => 'Page Include',
			'base' => 'gc_page_include',
			'description' => 'Include any page as a Visual Composer element',
			'class' => '',
			'category' => 'Content',
			'controls' => 'full',
			'icon' => 'icon-wpb-gc',
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => 'Page ID',
					'param_name' => 'post_id',
					'description' => 'Enter the ID of the page you would like to include.',
					'admin_label' => true
				),
				array(
					'type' => 'textfield',
					'heading' => 'Extra class name',
					'param_name' => 'el_class',
					'description' => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.'
				)
			) // params
		) );

		vc_map( array(
			'name' => 'Post List',
			'base' => 'gc_post_list',
			'description' => 'Display a customizable list of posts',
			'class' => '',
			'category' => 'Content',
			'controls' => 'full',
			'icon' => 'icon-wpb-gc',
			'params' => array(
				array(
					'type' => 'loop',
					'heading' => 'Query Builder',
					'param_name' => 'loop',
					'description' => 'Determine which posts should be returned based on these criteria.'
				),
				array(
					'type' => 'textarea_raw_html',
					'heading' => 'Template',
					'param_name' => 'template',
					'value' => '<li><a href="{{link}}">{{title}}</a></li>',
					'description' => 'Enter the HTML markup to be used for each item. If left empty, it will default to a simple bullet item. Available placeholders: author, content, date, excerpt, image:size:classes, link, title, acf_FIELD_NAME, event:date/date_time/venue. Usage: {{placeholder}}. Image sizes: thumbnail, medium, large, full.'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Excerpt Lenght',
					'param_name' => 'excerpt_length',
					'description' => 'Customize the length (in characters) of the excerpt to be displayed. Default: 55.'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Excerpt More Label',
					'param_name' => 'excerpt_more',
					'description' => 'Text to be used to link to the full article at the end of the excerpt.'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Wrapper HTML Tag',
					'param_name' => 'wrapper_tag',
					'value' => 'ul',
					'description' => 'If needed, you can choose a different HTML tag to wrap the list of items.'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Wrapper ID',
					'param_name' => 'wrapper_id',
					'description' => 'Specify the ID attribute for the wrapper tag.'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Extra class name',
					'param_name' => 'el_class',
					'description' => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.'
				),
				array(
					'type' => 'dropdown',
					'heading' => 'Date Column',
					'param_name' => 'date_query_column',
					'description' => 'Which column should be used to match your criteria.',
					'value' => array(
						'Post Date' => 'post_date',
						'Post Date GMT' => 'post_date_gmt',
						'Modified Date' => 'post_modified',
						'Modified Date GMT' => 'post_modified_gmt'
					),
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Posts Before',
					'param_name' => 'date_query_before',
					'description' => 'Find all the posts that were published earlier than this date. Format: YYYY-MM-DD.',
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Posts After',
					'param_name' => 'date_query_after',
					'description' => 'Find all the posts that were published after this date. Format: YYYY-MM-DD.',
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Meta Key',
					'param_name' => 'meta_key',
					'description' => 'Filter by custom field value.',
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Meta Value',
					'param_name' => 'meta_value',
					'description' => 'Filter by custom field value. Use date:keyword (for example, date:today) to dynamically calculate a date with strtotime.',
					'dependency' => array(
						'element' => 'meta_key',
						'not_empty' => true
					),
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Meta Value Comparison',
					'param_name' => 'meta_compare',
					'description' => 'Comparison operator for meta values.',
					'dependency' => array(
						'element' => 'meta_key',
						'not_empty' => true
					),
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'checkbox',
					'heading' => 'Sort by Meta Key above',
					'param_name' => 'sort_by_meta_key',
					'description' => 'Select this option to override the "orderby" option in the Query Builder.',
					'dependency' => array(
						'element' => 'meta_key',
						'not_empty' => true
					),
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Offset',
					'param_name' => 'offset',
					'description' => 'Exclude the first X posts from the output.',
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Exclude Post IDs',
					'param_name' => 'exclude_post_ids',
					'description' => 'Exclude given post IDs. Separate values with commas.',
					'group' => 'Advanced Filters'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Blog ID',
					'param_name' => 'blog_id',
					'description' => 'Retrieve posts from another blog in a multisite environment. Please contact your administrator to obtain this information.',
					'group' => 'Multisite'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Post Type',
					'param_name' => 'multisite_post_type',
					'description' => 'Determine what post type to retrieve (post, page, people, etc).',
					'dependency' => array(
						'element' => 'blog_id',
						'not_empty' => true
					),
					'group' => 'Multisite'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Category IDs',
					'param_name' => 'multisite_categories',
					'description' => 'Filter items by post category. Separate multiple values with commas. Enter a negative value to find items that don\'t have that term assigned to them (i.e., -23).',
					'dependency' => array(
						'element' => 'blog_id',
						'not_empty' => true
					),
					'group' => 'Multisite'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Tag IDs',
					'param_name' => 'multisite_tags',
					'description' => 'Filter items by post tag. Separate multiple values with commas. Enter a negative value to find items that don\'t have that term assigned to them (i.e., -44).',
					'dependency' => array(
						'element' => 'blog_id',
						'not_empty' => true
					),
					'group' => 'Multisite'
				),
				array(
					'type' => 'textfield',
					'heading' => 'Taxonomy Term IDs',
					'param_name' => 'multisite_tax_query',
					'description' => 'Filter items by term IDs. Prefix the list with a taxonomy slug: my_tax_slug: 15, 23, 24. Separate multiple values with commas. Enter a negative value to exclude that term (i.e., -16).',
					'dependency' => array(
						'element' => 'blog_id',
						'not_empty' => true
					),
					'group' => 'Multisite'
				)
			) // params
		) );
	}

	// Remove 'Private' and 'Protected' from page titles
	public static function remove_prefix_from_title( $_format ) {
		return '%s';
	}

	public static function admin_body_class( $classes ) {
		$vc_access_level = array();
		if ( current_user_can( 'manage_options' ) || current_user_can( 'cuny_full_vc' ) ) {
			$vc_access_level[] = 'cuny-full-vc';
		}
		if ( current_user_can( 'delete_pages' ) || current_user_can( 'cuny_editor_vc' ) ) {
			$vc_access_level[] = 'cuny-editor-vc';
		}
		$vc_access_level = implode(' ', $vc_access_level);

		if ( is_array( $classes ) ) {
			$classes[] = $vc_access_level;
		}
		else {
			$classes .= $vc_access_level;
		}

		return $classes;
	}

	public static function register_widgets() {
		register_widget( 'gc_page_include_widget' );
	}
}

class gc_page_include_widget extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'gc_page_include_widget',
			'GC Page Include',
			array( 'description' => "Include any page in a widget sidebar" )
		);
	}

	public function widget( $args, $instance ) {
		echo gc_visual_composer::page_include_shortcode( $instance, '' );
	}

	public function form( $instance ) {
		extract( shortcode_atts( array(
		  'post_id' => 0,
		  'el_class' => ''
		), $instance ) );

		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'post_id' ); ?>"><?php _e( 'Page ID' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'post_id' ); ?>" name="<?php echo $this->get_field_name( 'post_id' ); ?>" type="text" value="<?php echo esc_attr( $post_id ); ?>">
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'el_class' ); ?>"><?php _e( 'Extra Class' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'el_class' ); ?>" name="<?php echo $this->get_field_name( 'el_class' ); ?>" type="text" value="<?php echo esc_attr( $el_class ); ?>">
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance[ 'post_id' ] = intval( $new_instance[ 'post_id' ] );
		$instance[ 'el_class' ] = strip_tags( $new_instance[ 'el_class' ] );

		return $instance;
	}
}

// Init the plugin
add_action( 'plugins_loaded', array( 'gc_visual_composer', 'init' ), 10 );

